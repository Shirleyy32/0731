#include <iostream>
#include <cstring>
using namespace std;

class Student {
private:
    string name;//姓名
    int id;//学号
    static int count;//类内声明，类外初始化
    const int classNo;//班级编号
public:
    Student(string name, int id);//构造函数
    
    void Print();
    friend void Output(Student  s);
    
};
int Student::count = 45;
//常数据成员的初始化只能在类构造函数的初始化表中
Student::Student(string name, int id): classNo(1001) {
    this->name = name;
    this->id = id;
    count++;
}

void Student::Print() {
    cout << "名字:" << name << "\n" << "学号:" << id << "\n" << "班级:" << classNo << "\n" << "班级人数:" << count << endl;
}

void Output(Student s) {
    cout << "名字:" << s.name << "\n" << "学号:" << s.id << "\n" << "班级:" << s.classNo << "\n" << "班级人数:" << s.count
    << endl;
}

int main() {
    string nameA, nameB;
    cin >> nameA >> nameB;
    Student a(nameA, 1);
    Student b(nameB, 2);
    a.Print();//调用成员函数
    Output(b);//友元函数
    return 0;
}

