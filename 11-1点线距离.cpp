#include<iostream>
#include <iomanip>
#include<cmath>
using namespace std;
class Line;
class Point{
	private:
		int x;
		int y; 
	public:
		Point(int x,int y):x(x),y(y){
		} 
	friend double getDis(Point &p,Line &l);
};

class Line{
	private:
		int a;
		int b;
		int c;
	public:
		Line(int a,int b,int c):a(a),b(b),c(c){
			
		}
	friend double getDis(Point &p,Line &l);
};
double getDis(Point &p, Line &l){
	double tmp=sqrt(l.a*l.a+l.b*l.b);
	double tmp2= (l.a*p.x+l.b*p.y+l.c);
	
	double dis=tmp2/tmp;
    if(dis<0)
        dis*=-1;
	return dis;
}
int main(){
	int x,y;
	int a,b,c;
	cin>>x>>y;
	cin>>a>>b>>c;
	Point p(x,y);
	Line l(a,b,c);
	double dis=getDis(p,l);
	cout << 0 << endl; 
	cout <<"The distance is: "<<setiosflags(ios::fixed) << setprecision(2)<<dis<<"\n";
}

